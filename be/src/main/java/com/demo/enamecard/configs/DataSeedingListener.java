package com.demo.enamecard.configs;

import com.demo.enamecard.jwt.CustomPasswordEncoder;
import com.demo.enamecard.model.entities.RoleEntity;
import com.demo.enamecard.model.entities.AccountEntity;
import com.demo.enamecard.repositories.RoleRepository;
import com.demo.enamecard.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;

@Component
public class DataSeedingListener implements ApplicationListener<ContextRefreshedEvent> {

    private final AccountRepository accountRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public DataSeedingListener(AccountRepository accountRepository,
                               RoleRepository roleRepository,
                               PasswordEncoder passwordEncoder) {
        this.accountRepository = accountRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        RoleEntity roleEntityAdmin = roleRepository.findByName("Admin");
        RoleEntity roleEntityUser = roleRepository.findByName("User");
        if (roleEntityAdmin == null) {

            roleEntityAdmin = new RoleEntity();
            roleEntityAdmin.setName("Admin");
            roleEntityAdmin = roleRepository.save(roleEntityAdmin);
        }
        if (roleEntityUser == null) {

            roleEntityUser = new RoleEntity();
            roleEntityUser.setName("User");
            roleRepository.save(roleEntityUser);
        }

        if (accountRepository.findByUsername("admin") == null) {
            AccountEntity accountEntity = new AccountEntity();
            accountEntity.setUsername("admin");
            accountEntity.setPassword( passwordEncoder.encode("admin"));
            accountEntity.setFirstLogin(false);
            accountEntity.setRoleId(roleEntityAdmin.getId());
            accountEntity.setCreatedAt(OffsetDateTime.now());
            accountRepository.save(accountEntity);
        }
    }
}
