package com.demo.enamecard.jwt.service;

import com.demo.enamecard.jwt.model.CustomUserDetails;
import com.demo.enamecard.model.entities.AccountEntity;
import com.demo.enamecard.services.AccountService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class UserJwtService implements UserDetailsService {

    private final AccountService accountService;

    public UserJwtService(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AccountEntity accountEntity =  accountService.findUserByUsername(username);

        if (accountEntity == null){
            throw new UsernameNotFoundException(username);
        }
        return new CustomUserDetails(accountEntity);
    }

    public CustomUserDetails loadUserByUserId(Integer userId) {
        AccountEntity accountEntity = accountService.findUserById(userId);
        if (accountEntity == null) {
            throw new UsernameNotFoundException("userId" + userId);
        } else return new CustomUserDetails(accountEntity);
    }


}
