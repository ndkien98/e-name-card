package com.demo.enamecard.controller;

import com.demo.enamecard.model.request.ChangePasswordRequest;
import com.demo.enamecard.model.request.ForgotPasswordRequest;
import com.demo.enamecard.model.request.LoginRequest;
import com.demo.enamecard.model.response.SystemResponse;
import com.demo.enamecard.services.AccountService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/account")
@Validated
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping(value = "/login")
    public ResponseEntity<SystemResponse> login(@RequestBody LoginRequest loginRequest){
        return ResponseEntity.ok(accountService.login(loginRequest));
    }

    @PostMapping(value = "/logout")
    public ResponseEntity<SystemResponse> logout(){
        return ResponseEntity.ok(accountService.logout());
    }

    @PutMapping(value = "/password")
    public ResponseEntity<SystemResponse> changePassword (@RequestBody ChangePasswordRequest changePasswordRequest){
        return ResponseEntity.ok(accountService.changePassword(changePasswordRequest));
    }

    @PutMapping(value = "/password-mail")
    public ResponseEntity<SystemResponse> forGotPassword(@RequestBody ForgotPasswordRequest forgotPasswordRequest){
        return ResponseEntity.ok(accountService.forGotPassword(forgotPasswordRequest.getUsername()));
    }
}
