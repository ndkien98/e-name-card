package com.demo.enamecard.services;

import com.demo.enamecard.model.dto.AccountDTO;
import com.demo.enamecard.model.entities.AccountEntity;
import com.demo.enamecard.model.entities.ENameCardEntity;
import com.demo.enamecard.model.request.ChangePasswordRequest;
import com.demo.enamecard.model.request.LoginRequest;
import com.demo.enamecard.model.response.SystemResponse;

public interface AccountService {

    AccountEntity findUserByUsername(String username);

    AccountEntity findUserById(Integer id);

    SystemResponse login(LoginRequest loginRequest);

    SystemResponse logout();

    AccountDTO createAccountDefault(ENameCardEntity eNameCardEntity);

    SystemResponse changePassword(ChangePasswordRequest changePasswordRequest);

    SystemResponse forGotPassword(String username);

    void delete(String username);

    AccountEntity save (AccountEntity accountEntity);
}
