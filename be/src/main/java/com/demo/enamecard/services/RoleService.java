package com.demo.enamecard.services;

import com.demo.enamecard.model.entities.RoleEntity;

public interface RoleService {

    RoleEntity findByName(String name);

    RoleEntity findById(Integer id);
}
