package com.demo.enamecard.services;

import com.demo.enamecard.model.dto.AccountDTO;

public interface MailService {
    void sendMail(String emailResponsiblePerson, String subject, String content);

    void sendMailNotifyAccount(AccountDTO accountDTO);
}
