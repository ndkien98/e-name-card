package com.demo.enamecard.services.impl;

import com.demo.enamecard.exceptions.HttpErrorException;
import com.demo.enamecard.jwt.JwtTokenProvider;
import com.demo.enamecard.jwt.LoginSession;
import com.demo.enamecard.jwt.model.CustomUserDetails;
import com.demo.enamecard.mapper.AccountMapper;
import com.demo.enamecard.model.dto.AccountDTO;
import com.demo.enamecard.model.entities.AccountEntity;
import com.demo.enamecard.model.entities.ENameCardEntity;
import com.demo.enamecard.model.request.ChangePasswordRequest;
import com.demo.enamecard.model.request.LoginRequest;
import com.demo.enamecard.model.response.LoginResponse;
import com.demo.enamecard.model.response.SystemResponse;
import com.demo.enamecard.repositories.AccountRepository;
import com.demo.enamecard.services.AccountService;
import com.demo.enamecard.services.MailService;
import com.demo.enamecard.services.RoleService;
import com.demo.enamecard.utils.EncryptUtils;
import com.demo.enamecard.utils.StringResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository repository;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final PasswordEncoder passwordEncoder;
    private final LoginSession loginSession;
    private final RoleService roleService;
    private final MailService mailService;

    public AccountServiceImpl(AccountRepository accountRepository, AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider, PasswordEncoder passwordEncoder, LoginSession loginSession, RoleService roleService, MailService mailService) {
        this.repository = accountRepository;
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.passwordEncoder = passwordEncoder;
        this.loginSession = loginSession;
        this.roleService = roleService;
        this.mailService = mailService;
    }

    public AccountEntity findUserByUsername(String username) {
        return repository.findByUsername(username);
    }

    @Override
    public AccountEntity findUserById(Integer id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public SystemResponse login(LoginRequest loginRequest) {

        SystemResponse systemResponse = new SystemResponse();

        AccountEntity accountEntity = repository.findByUsernameAndPassword(loginRequest.getUsername(), passwordEncoder.encode(loginRequest.getPassword()));

        if (accountEntity == null) {
            systemResponse.setError(StringResponse.BAD_REQUEST).setStatus(HttpStatus.UNAUTHORIZED.value()).setData("username or password is wrong");
            return systemResponse;
        }

        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();

        loginSession.revokeLoginSession(String.valueOf(userDetails.getAccountEntity().getId()));

        String jwt = jwtTokenProvider.generateToken(userDetails);
        LoginResponse loginResponse = AccountMapper.INSTANCE.map(userDetails.getAccountEntity());
        loginResponse.setRoleName(roleService.findById(loginResponse.getRoleId()).getName());
        loginResponse.setToken(jwt);
        systemResponse.setData(loginResponse);
        systemResponse.setStatus(HttpStatus.OK.value());
        systemResponse.setError(StringResponse.OK);
        return systemResponse;
    }

    public SystemResponse logout() {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        loginSession.revokeLoginSession(String.valueOf(customUserDetails.getAccountEntity().getId()));

        SystemResponse systemResponse = new SystemResponse();
        systemResponse.setData(customUserDetails.getUsername() + " logged out");
        systemResponse.setStatus(HttpStatus.OK.value());
        systemResponse.setError(StringResponse.OK);
        return systemResponse;
    }

    public AccountDTO createAccountDefault(ENameCardEntity eNameCardEntity) {
        AccountEntity accountEntity = new AccountEntity();
        accountEntity.setUsername(eNameCardEntity.getPhone());
        String pass = EncryptUtils.getAlphaNumericString(10);
        accountEntity.setPassword(passwordEncoder.encode(pass));
        accountEntity.setFirstLogin(true);
        accountEntity.setRoleId(roleService.findByName("User").getId());
        accountEntity.setCreatedAt(OffsetDateTime.now());
        accountEntity.setEmail(eNameCardEntity.getEmail());
        accountEntity.setENameCardId(eNameCardEntity.getId());
        repository.save(accountEntity);

        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setUsername(accountEntity.getUsername());
        accountDTO.setPassword(pass);
        accountDTO.setEmail(eNameCardEntity.getEmail());
        return accountDTO;
    }

    @Override
    public SystemResponse changePassword(ChangePasswordRequest changePasswordRequest) {

        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AccountEntity accountEntity = customUserDetails.getAccountEntity();
        if (!repository.existsByUsernameAndPassword(accountEntity.getUsername(),passwordEncoder.encode(changePasswordRequest.getOldPassword()))){
            throw HttpErrorException.badRequest("old password is wrong");
        }
        accountEntity.setPassword(passwordEncoder.encode(changePasswordRequest.getNewPassword()));
//        loginSession.revokeLoginSession(String.valueOf(customUserDetails.getAccountEntity().getId()));
        accountEntity.setFirstLogin(false);
        repository.save(accountEntity);

        SystemResponse systemResponse = new SystemResponse();
        systemResponse.setStatus(HttpStatus.OK.value());
        systemResponse.setError("change password successful");
        return systemResponse;
    }

    @Override
    public SystemResponse forGotPassword(String username) {

        AccountEntity accountEntity = repository.findByUsername(username);
        if (accountEntity == null){
            throw HttpErrorException.badRequest("account not exists");
        }
        String pass = EncryptUtils.getAlphaNumericString(10);
        accountEntity.setPassword(passwordEncoder.encode(pass));
        accountEntity.setFirstLogin(true);
        repository.save(accountEntity);
        mailService.sendMail(accountEntity.getEmail(),"Thông cáo cấp lại mật khẩu","mật khẩu mới tại localhost:3000 là: " + pass);
        SystemResponse systemResponse = new SystemResponse();
        systemResponse.setStatus(HttpStatus.OK.value());
        systemResponse.setError("sent new password to your mail!");
        return systemResponse;
    }

    @Override
    public void delete(String username) {
        AccountEntity accountEntity = repository.findByUsername(username);
        repository.delete(accountEntity);
    }

    @Override
    public AccountEntity save(AccountEntity accountEntity) {
        return repository.save(accountEntity);
    }
}
