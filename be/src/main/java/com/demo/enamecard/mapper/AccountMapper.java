package com.demo.enamecard.mapper;

import com.demo.enamecard.model.entities.AccountEntity;
import com.demo.enamecard.model.response.LoginResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AccountMapper {

    AccountMapper INSTANCE = Mappers.getMapper(AccountMapper.class);

    LoginResponse map(AccountEntity accountEntity);
}
