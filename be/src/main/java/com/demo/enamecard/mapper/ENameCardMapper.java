package com.demo.enamecard.mapper;

import com.demo.enamecard.model.entities.ENameCardEntity;
import com.demo.enamecard.model.request.CreateENameCardRequest;
import com.demo.enamecard.model.request.EditNameCardRequest;
import com.demo.enamecard.model.response.ENameCardResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ENameCardMapper{
    ENameCardMapper INSTANCE = Mappers.getMapper(ENameCardMapper.class);

    @Mapping(source = "eNameCardEntity.originalFacebookLink",target = "faceBookLink")
    ENameCardResponse map(ENameCardEntity eNameCardEntity);

    List<ENameCardResponse> mapList(List<ENameCardEntity> eNameCardEntities);

    @Mapping(source = "createENameCardRequest.faceBookLink",target = "originalFacebookLink")
    ENameCardEntity map(CreateENameCardRequest createENameCardRequest);

    @Mapping(source = "editNameCardRequest.faceBookLink",target = "originalFacebookLink")
    ENameCardEntity map(EditNameCardRequest editNameCardRequest);

}
