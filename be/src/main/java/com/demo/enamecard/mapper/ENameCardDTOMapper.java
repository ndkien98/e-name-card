package com.demo.enamecard.mapper;

import com.demo.enamecard.model.dto.ENameCardDTO;
import com.demo.enamecard.model.entities.ENameCardEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ENameCardDTOMapper {

    ENameCardDTOMapper INSTANCE = Mappers.getMapper(ENameCardDTOMapper.class);

    ENameCardDTO map(ENameCardEntity eNameCardEntity);

    List<ENameCardDTO> mapList(List<ENameCardEntity> eNameCardEntities);

}
