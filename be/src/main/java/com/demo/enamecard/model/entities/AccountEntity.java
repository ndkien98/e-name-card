package com.demo.enamecard.model.entities;

import lombok.Data;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
@Table(name = "account")
@Data
public class AccountEntity {

    @Id
    @SequenceGenerator(name = "account_id_seq",sequenceName = "account_id_seq",allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "account_id_seq")
    private Integer id;

    @Column(unique = true)
    private String username;

    private String password;

    private boolean firstLogin;

    private OffsetDateTime createdAt;

    private int eNameCardId;

    private int roleId;

    @Column(unique = true)
    private String email;
}
