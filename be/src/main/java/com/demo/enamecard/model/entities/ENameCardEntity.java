package com.demo.enamecard.model.entities;

import lombok.Data;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
@Table(name = "e_name_card")
@Data
public class ENameCardEntity {

    @Id
    @SequenceGenerator(name = "e_name_card_id_seq",sequenceName = "e_name_card_id_seq",allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "e_name_card_id_seq")
    private Integer id;

    private String avatar;

    private String fullName;

    @Column(unique = true)
    private String phone;

    @Column(unique = true)
    private String email;

    private String originalFacebookLink;

    private String zaloLink;

    private String positions;

    @Column(columnDefinition = "timestamp default now()")
    private OffsetDateTime createdAt = OffsetDateTime.now();

    private OffsetDateTime updatedAt;

}
