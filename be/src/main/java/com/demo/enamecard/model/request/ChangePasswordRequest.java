package com.demo.enamecard.model.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class ChangePasswordRequest {

    private String newPassword;
    private String oldPassword;
}
