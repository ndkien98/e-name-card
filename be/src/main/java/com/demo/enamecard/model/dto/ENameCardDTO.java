package com.demo.enamecard.model.dto;

import lombok.Data;

@Data
public class ENameCardDTO {

    private String avatar;

    private String fullName;

    private String phone;

    private String email;

    private String faceBookLink;

    private String positions;
}
