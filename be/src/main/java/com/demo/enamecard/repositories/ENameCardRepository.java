package com.demo.enamecard.repositories;

import com.demo.enamecard.model.entities.ENameCardEntity;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface ENameCardRepository extends JpaRepository<ENameCardEntity,Integer>, JpaSpecificationExecutor<ENameCardEntity> {

    boolean existsByEmail(String email);

    boolean existsByPhone(String phone);

    @Query(value = "select exists(select * from e_name_card where id != :id and email = :email)",nativeQuery = true)
    boolean existsByEmailAndNotById(String email,int id);

    @Query(value = "select exists(select * from e_name_card where id != :id and phone = :phone)",nativeQuery = true)
    boolean existsByPhoneAndNotById(String phone,int id);

}
