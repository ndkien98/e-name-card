package com.demo.enamecard.repositories;

import com.demo.enamecard.model.entities.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<AccountEntity,Integer> {
    AccountEntity findByUsername(String username);

    AccountEntity findByUsernameAndPassword(String username, String password);

    boolean existsByUsernameAndPassword(String username, String password);
}
