package com.demo.enamecard.repositories;

import com.demo.enamecard.model.entities.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<RoleEntity,Integer> {

    RoleEntity findByName(String name);
}
