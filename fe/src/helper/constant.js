export const E_NAME_CARD_API_BASE_URL = "http://localhost:8080";

export const roleAdmin = "Admin";

export const baseLinkAPK = "fb://facewebmodal/f?href=";
export const baseLinkIOS = "fb://profile?id=";
export const baseLinkZalo = "https://zalo.me/";
export const baseOriginalLinkFbPhp = "https://www.facebook.com/profile.php?id=";
export const baseOriginalLinkFb = "https://www.facebook.com/";
